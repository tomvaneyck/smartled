#ifndef READING
#define READING

#include "mode.h"
#include "../adapterProperties.h"
#include "../packet.h"

#include <Arduino.h>
#include <ArduinoSTL.h>
#include <array.h>

/**
 * @brief Delivers the capabilities for reading data via the led.
 */
class Reading : public Mode {
  private:
    static constexpr int numberOfSamples = 3; ///< The amount of samples per bit.
    etl::array<bool, numberOfSamples> samples;
    int startIndex = 0;
    int sampleDelay; ///< The time spent reading a signle sample.

    int dischargeDelay = 100; ///< The amount of the time the led should discharge for.

    int environmentLight;         ///< The light level of the environment light.
    const int threshold = 100; ///< The threshold for the difference with the environment light.

    void calculateEnvironmentLight();

    int measureBitReadTime();
    int measureSampleReadTime();

    Packet readData();
    bool readSample();
    int readValue();

    bool shiftSamples();
    void printData(Packet data);
    int toByte(etl::array<bool, 8> bits);

    void notifySuccess();

  public:
    Reading(Adapter* adapter) : Mode(adapter), samples() {
      // Set up reading conditions.
      calculateEnvironmentLight();

      int difference = smartLed::bitDelay - measureBitReadTime();
      sampleDelay = difference / numberOfSamples;

      Serial.print("Environment light: ");
      Serial.println(environmentLight);
      Serial.print("Sample delay: ");
      Serial.println(sampleDelay);

      Serial.print("Time spent converting: ");
      Serial.println(Packet::measureExtractTime());

      Packet packet {};
      int start = micros();
      Encoder::isErrorFree(packet);
      int time = micros() - start;
      Serial.print("Time spent encoding: ");
      Serial.println(time);

      Serial.println();
    }

    virtual void startService() override;
    virtual bool executeServiceOnce(bool bit) override;

    bool readBit();
};

#endif