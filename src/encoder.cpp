#include "encoder.h"
#include "packet.h"

#include <ArduinoSTL.h>

FastCRC16 Encoder::crc16 = FastCRC16();

etl::array<bool, Encoder::crcLength> Encoder::calculateCRC(Packet packet)
{
    int size;
    if (packet.data.size() % 8 == 0) {
        size = packet.data.size() / 8;
    }
    else {
        size = packet.data.size() / 8 + 1;
    }
    uint8_t bytes[size];
    
    for (unsigned int i = 0; i < packet.data.size(); i += 8)
    {
        auto bits = etl::array<bool, 8>();
        bits.fill(0);

        for (unsigned int j = i; j - i < 8; j++)
        {
            bits[j % 8] = packet.data[j];
        }

        auto byte = toByte(bits);
        bytes[i / 8] = byte;
    }
    
    auto crc = crc16.ccitt(bytes, sizeof(bytes));
    auto crcArray = etl::array<bool, crcLength>();
    for (int i = 0; i < crcLength; i++) {
        crcArray[i] = crc & (1 << (crcLength - (i + 1)));
    }
    return crcArray;
}

uint8_t Encoder::toByte(etl::array<bool, 8> bits)
{
    uint8_t byte = 0;
    for (unsigned int i = 0; i < bits.size(); i++)
    {
        if (bits[i])
        {
            byte |= 1 << (bits.size() - (i + 1));
        }
    }
    return byte;
}

bool Encoder::isErrorFree(Packet packet)
{
    return calculateCRC(packet) == packet.crc;
}