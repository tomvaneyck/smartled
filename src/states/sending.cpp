#include "sending.h"
#include "../adapter.h"
#include "../encoder.h"

#include <ArduinoSTL.h>
#include <array.h>

void Sending::startService()
{
    adapter->setSendingMode();

    Packet packet {};

    while (true)
    {
        if (Serial.available() > 0)
        {
            auto byte = Serial.read();
            auto bits = toBits(byte);

            for (unsigned int i = 0; i < bits.size(); i++) {
                packet.data[(packet.dataLength * 8) + i] = bits[i];
            }
            packet.dataLength++;

            Serial.write(byte);

            if (byte == '\n') {
                packet.packetFollowing = false;
                packet.crc = Encoder::calculateCRC(packet);
                
                sendData(packet);
                packet.sequenceBit = !packet.sequenceBit;
                packet.dataLength = 0;

                adapter->setReadingMode();
                adapter->startService();
            }
            else if (packet.dataLength == 4) {
                packet.packetFollowing = true;
                packet.crc = Encoder::calculateCRC(packet);
                
                sendData(packet);
                packet.sequenceBit = !packet.sequenceBit;
                packet.dataLength = 0;
            }
        }
    }
}

etl::array<bool, 8> Sending::toBits(int byte)
{
    etl::array<bool, 8> bits {};
    for (unsigned int i = 0; i < bits.size(); i++)
    {
        bits[i] = byte & (1 << (bits.size() - (i + 1)));
    }
    return bits;
}

/**
 * @brief Sends the given bit via the led.
 * 
 * This function is pretty useless when called with parameter 0, as it just
 * leaves the led off for 1 bitDelay.
 * 
 * @param bit The bit to send.
 */
void Sending::sendBit(bool bit)
{
    digitalWrite(smartLed::sendPin, bit);
    delayMicroseconds(smartLed::bitDelay);
    digitalWrite(smartLed::sendPin, LOW);
}

void Sending::sendData(Packet packet)
{
    digitalWrite(smartLed::readyPin, LOW);
    
    auto bitStream = packet.generateBitStream();

    do {
        // Start of frame.
        digitalWrite(smartLed::sendPin, HIGH);
        delayMicroseconds(smartLed::bitDelay);
        
        for (auto it = bitStream.begin(); it != bitStream.end(); it++) {
            digitalWrite(smartLed::sendPin, *it);
            delayMicroseconds(smartLed::bitDelay);
        }

        digitalWrite(smartLed::sendPin, LOW);

        // Wait time receiver needs to extract packet.
        delayMicroseconds(packetExtractionDelay);
    } while (!receivedErrorFree());

    digitalWrite(smartLed::readyPin, HIGH  );
}

bool Sending::receivedErrorFree()
{
    adapter->setReadingMode();
    bool answer = adapter->executeServiceOnce(true);
    answer |= adapter->executeServiceOnce(true);
    answer |= adapter->executeServiceOnce(true);
    adapter->setSendingMode();
    return answer;
}

bool Sending::executeServiceOnce(bool bit) {
    sendBit(bit);
    return true;
}