#ifndef SENDING
#define SENDING

#include "mode.h"
#include "../packet.h"

#include <Arduino.h>
#include <ArduinoSTL.h>

/**
 * @brief Delivers the capabilities for sending data via the led.
 */
class Sending : public Mode {
  private:
    int packetExtractionDelay;
  
    etl::array<bool, 8> toBits(int byte);

    bool receivedErrorFree();

  public:
    Sending(Adapter* adapter): Mode(adapter) {
      packetExtractionDelay = Packet::measureExtractTime();
      Serial.print("Packet extraction delay: ");
      Serial.println(packetExtractionDelay);
      Serial.println();
    }

    virtual void startService() override;
    virtual bool executeServiceOnce(bool bit) override;
  
    void sendBit(bool bit);
    void sendData(Packet data);
};

#endif