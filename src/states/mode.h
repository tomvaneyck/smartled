#ifndef MODE
#define MODE

class Adapter;

class Mode {
  protected:
    Adapter* const adapter; ///< Const pointer to Adapter. See: https://stackoverflow.com/a/21476937/4306332

  public:
    Mode(Adapter* adapter) : adapter(adapter) {}
    virtual ~Mode() = default;

    virtual void startService() = 0; ///< Starts the service.
    virtual bool executeServiceOnce(bool bit) = 0; ///< Executes the service on one single bit.
}; // interface Mode

#endif