#include "reading.h"
#include "../adapter.h"
#include "../packet.h"
#include "../encoder.h"

#include <ArduinoSTL.h>

/**
 * @brief Sets up and starts the reader.
 * 
 * Measures the time spent on reading one sample and calculates the delay for a sample.
 * Then starts the reading process.
 */
void Reading::startService()
{
    while (true)
    {
        if (readSample())
        {
            digitalWrite(smartLed::readyPin, LOW);
            auto data = readData();
            printData(data);

            // Switch mode if last packet
            if (!data.packetFollowing) {
                adapter->setSendingMode();
                adapter->startService();
            }

            digitalWrite(smartLed::readyPin, HIGH);
        }
    }
}

int Reading::measureBitReadTime() {
    int start = micros();
    readBit();
    int time = micros() - start;
    return time;
}

/**
 * @brief Measures the the time spent reading one sample.
 * 
 * @return int the time spent reading in microseconds.
 */
int Reading::measureSampleReadTime()
{
    int start = micros();
    readValue();
    int time = micros() - start;
    return time;
}

/**
 * @brief Reads incoming data.
 * 
 * Reads the specified amount of bits for one adapter, followed by the specified
 * amount of crc bits. Then checks if the received data and crc correspond and
 * returns a confirmation signal to the sender. 
 */
Packet Reading::readData()
{
    Packet packet {};
    
    do {
        // Start of frame.
        delayMicroseconds(smartLed::bitDelay);
        
        etl::vector<bool, smartLed::maxPacketLength> bitStream {};

        for (unsigned int i = 0; i < Packet::size(); i++) {
            bitStream.push_back(readBit());
        }

        packet = Packet::extractPacket(bitStream);

    } while(!Encoder::isErrorFree(packet));

    notifySuccess();

    return packet;
}

/**
 * @brief Reads a single bit from the sender.
 * 
 * @return `true` if the bit received was 1.
 * @return `false` if the bit received was 0.
 */
bool Reading::readBit()
{
    for (int sample = startIndex; sample < numberOfSamples; sample++)
    {
        samples[sample] = readSample();
        delayMicroseconds(sampleDelay);
    }

    return shiftSamples();
}

/**
 * @brief Shifts the samples following the oversampling pattern and returns the
 * resulting bit.
 * 
 * If the first sample is different from the other two, assume that the reader
 * is reading faster than the sender is sending. Thus, wait one sample delay to
 * eliminate the lead and take the middle sample as the value of the actual
 * bit.
 * 
 * If the last sample is different, assume that the reader is reading slower
 * than the sender is sending. Thus, skip the reading of one sample the next
 * time a bit is read and take the middle sample as the value of the actual
 * bit.
 *  
 * Ignores the case where the middle of the three samples is different from the
 * other two.
 * 
 * @return The value of the read bit, determined following the oversampling
 * pattern. 
 */
bool Reading::shiftSamples()
{
    int sum = samples[0] * 1;
    sum += samples[1] * 2;
    sum += samples[2] * 4;

    bool bit;
    switch (sum)
    {
    case 1: // 100
    case 6: // 011
    {
        startIndex = 0;
        bit = samples[1];
        delayMicroseconds(sampleDelay);
        break;
    }
    case 4: // 001
    case 3: // 110
    {
        startIndex = 1;
        samples[0] = samples[2];
        bit = samples[1];
        break;
    }
    case 0: // 000
    case 2: // 010
    case 5: // 101
    case 7: // 111
    {
        startIndex = 0;
        bit = samples[0];
        break;
    }
    }

    return bit;
}

/**
 * @brief Reads a single sample.
 * 
 * Reads the light level and compares it to the environment light to determine
 * if the difference with the read value is greater or smaller than the threshold.
 * 
 * @return `true` if the difference with the environment light is greater than
 * the threshold.
 * @return `false` if the difference with the environment light is smaller than
 * the threshold.
 */
bool Reading::readSample()
{
    int value = readValue();
    return environmentLight - value > threshold;
}

/**
 * @brief Reads the light value currently falling onto the led.
 * 
 * @return int The light value falling onto the led.
 */
int Reading::readValue()
{
    pinMode(smartLed::readPin, OUTPUT);
    digitalWrite(smartLed::readPin, HIGH);
    pinMode(smartLed::readPin, INPUT);
    delayMicroseconds(dischargeDelay);
    return analogRead(smartLed::readPin);
}

/**
 * @brief Prints the data to the terminal.
 * 
 * @param packet The packet that contains the data.
 */
void Reading::printData(Packet packet)
{
    for (unsigned int i = 0; i < Packet::maxDataLength / 8 && i < packet.dataLength; i++) {
        etl::array<bool, 8> bits {};
        for (int j = 0; j < 8; j++) {
            bits[j] = packet.data[(i * 8) + j];
        }
        auto byte = toByte(bits);
        Serial.write(byte);
    }
}

/**
 * @brief Converts the bit array to a byte.
 * 
 * @param bits The bit array to convert.
 * @return int The byte consisting of the bits given a sparameter.
 */
int Reading::toByte(etl::array<bool, 8> bits)
{
    int byte = 0;
    for (unsigned int i = 0; i < bits.size(); i++)
    {
        if (bits[i])
        {
            byte |= 1 << (bits.size() - (i + 1));
        }
    }
    return byte;
}

/**
 * @brief Calculates and sets the average environment light value.
 */

/* Dit is een test*/
void Reading::calculateEnvironmentLight()
{
    int sum = 0;

    for (int i = 0; i < 10; i++)
    {
        pinMode(smartLed::readPin, OUTPUT);
        digitalWrite(smartLed::readPin, HIGH);
        pinMode(smartLed::readPin, INPUT);
        delayMicroseconds(dischargeDelay);
        sum += analogRead(smartLed::readPin);
    }

    environmentLight = sum / 10;
}

/**
 * @brief Sends a signal back to the sender to notify him of correct receival
 * of data.
 */
void Reading::notifySuccess()
{
    // Setting the smartLed::sendPin high for 1 bitdelay.
    adapter->setSendingMode();
    adapter->executeServiceOnce(true);
    adapter->setReadingMode();
}

bool Reading::executeServiceOnce(bool bit) {
    bool answer = readSample();
    return answer;
}