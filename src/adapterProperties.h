#ifndef ADAPTER_PROPERTIES
#define ADAPTER_PROPERTIES

#include <Arduino.h>

namespace smartLed
{
constexpr int readyPin = 13; ///< The pin on which the ready led resides.
constexpr int readPin = A0; ///< The analog pin on which the led cathode is connected.
constexpr int sendPin = 8;  ///< The pin on which the led anode is connected.

constexpr int bitDelay = 1000; ///< The amount of microseconds one bit takes to transmit.

constexpr int maxPacketLength = 100; ///< The max length of a packet in bits.
} // smartLed


#endif