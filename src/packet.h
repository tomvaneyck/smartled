#ifndef PACKET
#define PACKET

#include "adapterProperties.h"
#include "encoder.h"

#include <ArduinoSTL.h>
#include <array.h>
#include <vector.h>
#include <algorithm.h>

struct Packet {
  ///< The max length of the data section of a packet on this transmission channel.
  static constexpr int maxDataLength = 32; // 4 bytes.
  static bool sequenceBit; ///< Switches with every subsequent packet to enforce alternation.
  bool packetFollowing = 0; ///< Denotes if another packet is directly following this one.
  unsigned int dataLength = 0; ///< The actual length of the data, in bytes, contained in this packet.
  etl::array<bool, maxDataLength> data {}; ///< The data contained in this packet.
  etl::array<bool, Encoder::crcLength> crc {}; ///< The crc value for this data.

  Packet() : dataLength(0) {}

  /**
   * @brief Generates the length of the generated bit stream of this packet.
   * 
   * @return unsigned int The length of the generated bit stream.
   */
  static inline unsigned int size() {
    // sequenceBit + packetFollowing + dataLength + data + crc.
    int size = 1 + 1 + 3 + maxDataLength + Encoder::crcLength;
    // and also bit stuffing bits.
    return size + size / 3;
  }

  inline etl::vector<bool, smartLed::maxPacketLength> generateBitStream() {
    etl::vector<bool, smartLed::maxPacketLength> bitStream;
    bitStream.push_back(sequenceBit);
    bitStream.push_back(packetFollowing);
    bitStream.push_back(dataLength & 4);
    bitStream.push_back(dataLength & 2);
    bitStream.push_back(dataLength & 1);
    bitStream.insert(bitStream.end(), data.begin(), data.end());
    bitStream.insert(bitStream.end(), crc.begin(), crc.end());

    // Add a bit after three bits (bit stuffing).
    int count = 1;
    for (auto it = bitStream.begin(); it != bitStream.end(); it++, count++) {
      if (count % 3 == 0) {
        it = bitStream.insert(it + 1, !*it);
      }
    }

    return bitStream;
  }

  static inline Packet extractPacket(etl::vector<bool, smartLed::maxPacketLength> bitStream) {
    // Remove a bit after three bits (bit stuffing).
    int count = 1;
    for (auto it = bitStream.begin(); it != bitStream.end(); count++) {
      if (count % 3 == 0) {
        it = bitStream.erase(it + 1);
      } else {
        it++;
      }
    }

    auto it = bitStream.begin();

    Packet packet {};
    
    packet.sequenceBit = *it++;
    packet.packetFollowing = *it++;
    packet.dataLength = 0;
    packet.dataLength |= *it++ << 2;
    packet.dataLength |= *it++ << 1;
    packet.dataLength |= *it++;
    etl::copy_n(it, maxDataLength, packet.data.begin());
    etl::copy_n(it + maxDataLength, Encoder::crcLength, packet.crc.begin());

    return packet;
  }

  static inline int measureExtractTime() {
    // Setup
    etl::vector<bool, smartLed::maxPacketLength> bitStream {};
    for (unsigned int i = 0; i < Packet::size(); i++) {
        bitStream.push_back(i%2);
    }
    
    int start = micros();
    
    Packet::extractPacket(bitStream);
    
    return micros() - start;
  }
};

#endif