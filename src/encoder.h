#ifndef ENCODER
#define ENCODER

#include "adapterProperties.h"

#include <ArduinoSTL.h>
#include <array.h>
#include <FastCRC.h>

struct Packet;

class Encoder
{
  private:
    static FastCRC16 crc16;

    static uint8_t toByte(etl::array<bool, 8> bits);

  public:
    Encoder() = delete;

    static constexpr int crcLength = 16;

    static etl::array<bool, crcLength> calculateCRC(Packet packet);
    static bool isErrorFree(Packet packet);
};

#endif