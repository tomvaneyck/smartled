#ifndef ADAPTER
#define ADAPTER
  
#include "adapterProperties.h"
#include "states/reading.h"
#include "states/sending.h"

#include <Arduino.h>
#include <functional.h>
#include <memory.h>
  
class Adapter
{
  private:
    bool isSetup = false;
    Mode* mode = nullptr;
    Reading reader = Reading(this);
    Sending sender = Sending (this);

    inline void resetPins()
    {
        pinMode(smartLed::readPin, OUTPUT);
        digitalWrite(smartLed::readPin, LOW);
        pinMode(smartLed::sendPin, OUTPUT);
        digitalWrite(smartLed::sendPin, LOW);
    }

  public:
    inline void setReadingMode() {
      mode = &reader;
      resetPins();
    }

    inline void setSendingMode() {
      mode = &sender;
      resetPins();
    }

    inline void startService() {
      if (mode != nullptr) {
        mode->startService();
      }
    }

    inline bool executeServiceOnce(bool bit) {
      if (mode != nullptr) {
        return mode->executeServiceOnce(bit);
      }
      return true;
    }
}; // class Adapter

#endif