# SmartLed

A library that delivers reading and sending capabilities for the Smart Led. This library uses [Embedded Template Library](https://github.com/ETLCPP/etl) as an implementation of std c++ functions for embedded platforms.

## Project

The code in this repository was created as part of my bachelor thesis at KULeuven. The report for this thesis can be found [here](verslag.pdf), but is written in Dutch.